// number 3 and 4
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	 let title = data.map(element => element.title)
	 console.log(title);
});



// number 5 and 6
fetch('https://jsonplaceholder.typicode.com/todos/1/comments', {
	method: 'GET',
})
.then(res => res.json())
.then(data => {console.log(data)})


// number 7
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created To Do List Item',
		completed: true,
		userId: 2
	})
})
.then(response => response.json())
.then(json => console.log(json));


// number 8 and 9
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json',
	},
	body: JSON.stringify({
		title: "Update To Do List Item",
		description: "To update my to do list with a different data structured",
		status: "Pending",
		dateCompleted: "Pending",
		userId: 1
	})
})
.then(res => res.json())
.then(data => console.log(data));



// number 10 and 11
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json',
	},
	body: JSON.stringify({
		status: "Complete",
		dateCompleted: "07/09/21"
	})
})
.then(res => res.json())
.then(data => console.log(data));


// number 12
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',

})
.then(res => res.json())
.then(data => console.log(data));